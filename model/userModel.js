var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var userSchema = new mongoose.Schema({
  login: {type:String, lowercase:true, unique:true,required:true},
  pwd: {type: String, required: true},
  todo: [String]
});

//authenticate input against database
userSchema.statics.authenticate = (login, pwd, callback) => {
    userModel.findOne({ login: login })
        .exec( (err, user) => {
            console.log('auth_bcrypt-user',user)
        if (err) {
            return callback(err)
        } else if (!user) {
            var err = new Error('User not found.');
            err.status = 401;
            return callback(err);
        }
        bcrypt.compare(pwd, user.pwd, (err, result) => {
            console.log('bcrypt result',result)
            if (result === true) return callback(null, user);
            else return callback();
        })
    });
}

//PRE HOOK : hashing a password before saving it to the database
userSchema.pre('save', function (next) {
    var user = this;
    bcrypt.hash(user.pwd, 10, function (err, hash){
        if (err) {
        return next(err);
        }
        user.pwd = hash;
        next();
    })
});

var userModel = mongoose.model('user', userSchema);
module.exports = userModel;


  

