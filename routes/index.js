var express = require('express');
var router = express.Router();
var user = require('../model/userModel');

/* GET home page. */
router.get('/', (req, res) => res.render('index', { title: 'Wario!'}))

router.get('/todo', function(req,res,next){
  //console.log('user in session:',req.session.userId)
  user.findById(req.session.userId)
  .exec((error,user)=>{
    if (error) return console.log(error);
    if (user === null) return res.redirect('/users/login');
    return res.render('todo', {title:'Todo',todo: user.todo})
  })
});

router.get('/todo/sort', function(req,res,next){
  const sort = req.query.sort
  const tmp = req.session.todolist;
  const tmp2 = []
  for (x in sort) tmp2.push(tmp[sort[x]])
  req.session.todolist = tmp2
  res.send('ok');
})

router.post('/todo', function(req,res,next){
  user.findByIdAndUpdate({_id:req.session.userId},
    {$push: {todo: req.body.newtodo}}).then((doc)=> {return res.redirect('/todo')})
});

router.put('/todo', function(req,res,next){
  req.session.todolist[req.body.todo] = req.body.put_todo;
  res.redirect('/todo');
})

router.delete('/todo', function(req,res,next){
  user.findByIdAndUpdate({_id:req.session.userId},
    {$pull:{todo : req.body.todo}})
    .then((doc)=>{return res.redirect('/todo')})
});

module.exports = router;
