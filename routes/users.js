var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var user = require('../model/userModel');


router.get('/login', function(req, res, next) {
  res.render('login',{title:"Login",msg:''});
})
router.get('/register', function(req, res, next) {
  res.render('register',{title:"Register"});
})
router.post('/login', function(req,res,next){
  if ((req.body.login)&&(req.body.pwd)){
    user.authenticate(req.body.login, req.body.pwd,(error,user)=>{
      if (error || !user){
        var err = new Error('Wrong email or password.');
        err.status = 401;
        return next(err);
      }
      console.log('_id:',user._id);
      req.session.userId = user._id
      return res.redirect('/todo');
    }) 
  } else return res.redirect('/users/login');
})
router.post('/register', function(req,res,next){
  //console.log(req.body);
  if ((req.body.login)&&(req.body.pwd)){
    let u = new user({login:req.body.login, pwd:req.body.pwd});
    u.save().then(
      (doc)=> res.redirect('/users/login'),
      (err)=> res.redirect('/users/register')
    );
  }
    
})

module.exports = router;
