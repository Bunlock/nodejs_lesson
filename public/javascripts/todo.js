$( function() {
    console.log('page reload')

    $( "#sortable" ).sortable({
      revert: true,
      update: ()=>{
        let sort = [];
        $.each($('#sortable').find('li'), function() {
            sort.push(parseInt($(this).find('input:first').val()))
            
        });
        console.log(sort);
        $.ajax({
          url:  '/todo/sort',
          method:'get',
          dataType: 'json',
          data: {sort},
          success: (data)=>{
              window.reload();
          }
        })
      }
    });
    $( "ul, li" ).disableSelection();
});